import React, { Component } from 'react'
import { connect } from 'react-redux'
import Button from '../../../components/UI/Button/Button'
import classes from './ContactData.css'

import axios from '../../../axios-orders'
import Spinner from '../../../components/UI/Spinner/Spinner'
import Input from '../../../components/UI/Input/Input'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import * as actionCreators from '../../../store/actions/index'

class ContactData extends Component {

    state = {
        orderForm : {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-Mail'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ZIP Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'fastest', displayValue: 'Fastest'},
                        {value: 'cheapest', displayValue: 'Cheapest'}
                    ]
                },
                value: 'fastest',
                valid: true
            }
        },
        formIsValid: false
    }

    checkValidity(value, rules) {
        let isValid = false
        if(!rules) {
            return true
        }
        if(rules.required) {
            isValid = value.trim() !== ''
        }
        if(rules.minLength) {
            isValid = isValid && value.length >= rules.minLength
        }
        if(rules.maxLength) {
            isValid = isValid && value.length <= rules.maxLength
        }
        return isValid
    }

    inputChangeHandler = (event, inputIdentifier) => {
        const updatedOrderForm = {
            ...this.state.orderForm,
        }
        const updatedFormEle = {
            ...updatedOrderForm[inputIdentifier]
        }
        updatedFormEle.value = event.target.value
        updatedFormEle.valid = this.checkValidity(updatedFormEle.value, updatedFormEle.validation)
        updatedFormEle.touched = true
        console.log(updatedFormEle)
        updatedOrderForm[inputIdentifier] = updatedFormEle

        let formIsValid = true
        for (let inputEle in updatedOrderForm) {
            formIsValid = formIsValid && updatedOrderForm[inputEle].valid
        }

        console.log(formIsValid)

        this.setState({orderForm: updatedOrderForm, formIsValid: formIsValid})
    }

    orderHandler = (event) => {
        event.preventDefault()
        const formData = {}
        for (let formEle in this.state.orderForm) {
            formData[formEle] = this.state.orderForm[formEle].value
        }
        const ingredients = this.props.ings
        const order = {
            ingredients: ingredients,
            totalPrice: this.props.price,
            orderData: formData
        }
        this.props.onOrderBurger(order)
    }

    render() {
        const formElementsArray = []
        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            })
        }
        let form = (
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map((formEle) => (
                    <Input key={formEle.id} elementType={formEle.config.elementType} elementConfig={formEle.config.elementConfig} value={formEle.config.value} invalid={!formEle.config.valid} shouldValidate={formEle.config.validation} touched={formEle.config.touched} changed={(event) => this.inputChangeHandler(event, formEle.id)} />
                ))}
                <Button btnType="Success" disabled={!this.state.formIsValid}>ORDER</Button>
            </form>
        );
        if(this.props.loading) {
            form = <Spinner />
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.order.loading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger: (orderData) => dispatch(actionCreators.purchaseBurger(orderData))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios))