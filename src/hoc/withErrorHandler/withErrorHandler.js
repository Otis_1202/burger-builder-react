import React, { Component } from 'react'

import Modal from '../../components/UI/Modal/Modal'
import Aux from '../Auxilliary/Auxilliary'

const withErrorHandler = (WrappedComponent, axios) => {
    
    return class extends Component {

        state = {
            error: null
        }

        /**
         * When you use componentWillMount, this, handler will be only fire after WrappedComponent mounted, it's mean you can't handler error when the WrappedComponent rendering...
         */
        componentWillMount () {
            axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            });
            axios.interceptors.response.use(res => res, error => {
                this.setState({error: error});
            });
        }

        /**
         * Remove interceptor when component unmounted to save memory
         */
        componentWillUnmount() {
            try {
                console.log(axios.interceptors.request)
                axios.interceptors.request.eject(this.reqInterceptor)
                axios.interceptors.response.eject(this.resInterceptor)
            } catch (error) {
                console.log("Error when reject interceptors")
                console.log(error)
            }
            
        }

        errorConfirmedHandler = () => {
            this.setState({error: null})
        }

        render() {
            return (
                <Aux>
                    <Modal 
                        show={this.state.error}
                        modalClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            );
        }
    }

}

export default withErrorHandler